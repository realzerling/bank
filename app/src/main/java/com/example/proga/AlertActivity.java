package com.example.proga;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;

import android.app.Activity;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

public class AlertActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alert);
    }

    public void myClick(View view) {
        MyDialogFragment myDialogFragment = new MyDialogFragment();

        FragmentManager manager = getSupportFragmentManager();
        myDialogFragment.show(manager,"MyDialog");

    }
}