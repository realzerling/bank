package com.example.proga;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;

import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void LoginClick(View view) {
        MyDialogFragment DialogFragment = new MyDialogFragment();
        FragmentManager manager = getSupportFragmentManager();
        DialogFragment.show(manager,"MyDialog");
    }
}